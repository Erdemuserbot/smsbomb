from google.colab import _message as google_message

# Function to check if the session is still open
def is_session_open():
    try:
        google_message.blocking_request(
            "get_ipython()",
            request_type=google_message.RequestType.GET_VARIABLES
        )
        return True
    except:
        return False

# Function to reopen the session
def reopen_session():
    google_message.blocking_request(
        "notebook_reload",
        request_type=google_message.RequestType.VOID
    )

# Continuously check the session status and reopen if necessary
while True:
    if not is_session_open():
        reopen_session()
    time.sleep(60)  # Wait for 1 minute before checking again
